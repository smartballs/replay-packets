package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/smartballs/replay-packets/config"
	replaypackets "gitlab.com/smartballs/replay-packets/internal/replay-packets"
	"net"
	"strconv"
)

func main() {

	address := config.Common.AddressIp + ":" + strconv.Itoa(config.Common.AddressPort)

	connection, err := net.Dial(config.Common.NetworkType, address)
	if err != nil {
		log.Fatalf("Dial failed. Network: [%s], Address: [%s]", config.Common.NetworkType, address)
	}
	defer func() {
		err = connection.Close()
		if err != nil {
			log.Error("Unable to close properly the connection")
		}
	}()

	replaypackets.Start(&connection)

}
