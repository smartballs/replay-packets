module gitlab.com/smartballs/replay-packets

go 1.13

require (
	github.com/google/gopacket v1.1.17
	github.com/sirupsen/logrus v1.4.2
)
