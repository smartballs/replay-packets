
all: dep build run

dep:
	go mod download
build:
	GOOS=linux GO111MODULE=on go build gitlab.com/smartballs/replay-packets/cmd/replay-packets
run:
	go run gitlab.com/smartballs/replay-packets/cmd/replay-packets
test:
	go test gitlab.com/smartballs/replay-packets/... -v -coverprofile .coverage.txt
	go tool cover -func .coverage.txt
coverage: test
	go tool cover -html=.coverage.txt