package replaypackets

import (
	"fmt"
	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
	log "github.com/sirupsen/logrus"
	"gitlab.com/smartballs/replay-packets/config"
	"net"
	"path/filepath"
	"time"
)

func Start(connection *net.Conn) {
	if len(config.Common.FileUniquePath) > 0 {
		log.Infof("Handle a unique file : %s", config.Common.FileUniquePath)

		packets := getPackets(config.Common.FileUniquePath)
		send(connection, packets)
	} else {
		pattern := StripTrailingSlash(config.Common.AssetsPath) + "/*." + config.Common.FileType

		files, err := filepath.Glob(pattern)
		if err != nil {
			log.Fatalf("Cannot load pcap files with pattern : %s", pattern)
		}

		for _, file := range files {
			packets := getPackets(file)
			send(connection, packets)
			delay()
		}
	}
}

func send(connection *net.Conn, handle *pcap.Handle) {
	packetSource := gopacket.NewPacketSource(handle, handle.LinkType())
	lastTimestamp := 0

	for packet := range packetSource.Packets() {
		currentTimestamp := packet.Metadata().Timestamp.Nanosecond()
		if lastTimestamp == 0 {
			lastTimestamp = currentTimestamp
		}
		delay := currentTimestamp - lastTimestamp

		time.Sleep(time.Duration(delay) * time.Nanosecond)
		_, _ = fmt.Fprintf(*connection, "%s", packet.ApplicationLayer().LayerContents())

		lastTimestamp = currentTimestamp
	}
}

func getPackets(path string) *pcap.Handle {
	log.Infof("Reading path: %s", path)

	handle, err := pcap.OpenOffline(path)
	if err != nil {
		log.Fatalf("Unable to open pcap path: %s", path)
	}

	return handle
}

func delay() {
	time.Sleep(time.Duration(config.Common.FileDelay) * time.Second)
}

// Strips the trailing slash on the provided path if there's one.
func StripTrailingSlash(path string) string {

	lastCharacterIndex := len(path) - 1
	if lastCharacterIndex < 0 {
		return path
	}

	if path[lastCharacterIndex] == '/' {
		runes := []rune(path)
		stripPath := string(runes[0:lastCharacterIndex])

		return stripPath
	} else {
		return path
	}
}
