package replaypackets_test

import (
	replaypackets "gitlab.com/smartballs/replay-packets/internal/replay-packets"
	"testing"
)

func TestStripTrailingSlash(t *testing.T) {
	type dataItems struct {
		input    string
		expected string
	}

	data := []dataItems{
		{"/", ""},
		{"/data", "/data"},
		{"", ""},
		{"//", "/"},
		{"/data/", "/data"},
	}

	for _, item := range data {
		result := replaypackets.StripTrailingSlash(item.input)

		if result != item.expected {
			t.Errorf("expected: '%s' got:'%s' with: '%s'", item.expected, result, item.input)
		}
	}

}
