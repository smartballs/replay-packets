package cfg

import (
	"fmt"
	"os"
	"strconv"
)

// Environment string config param

type Str string

func (env Str) ByDefault(def string) string {

	s, b := os.LookupEnv(string(env))

	if b {
		return s
	} else {
		return def
	}
}

func (env Str) WithoutDefault() string {

	s, b := os.LookupEnv(string(env))

	if !b {
		_, _ = fmt.Fprintf(os.Stderr, "Missing string environment variable: %s", env)
		os.Exit(1)
	}

	return s
}

// Environment numerical config param

type Num string

func (env Num) ByDefault(def int) int {
	envInt, b := os.LookupEnv(string(env))

	i, e := strconv.ParseInt(envInt, 10, 64)
	if b && e != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Unable to convert environment variable '%s' to integer value was: '%s'", env, envInt)
		os.Exit(1)
	}

	if !b {
		return def
	} else {
		return int(i)
	}
}

func (env Num) WithoutDefault() int {
	envInt, b := os.LookupEnv(string(env))

	if !b {
		_, _ = fmt.Fprintf(os.Stderr, "Missing integer environment variable: %s", env)
		os.Exit(1)
	}

	i, e := strconv.ParseInt(envInt, 10, 64)
	if e != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Unable to convert environment variable '%s' to integer value was: '%s'", env, envInt)
		os.Exit(1)
	}

	return int(i)
}

// Environment Boolean config param

type Boolean string

func (env Boolean) ByDefault(def bool) bool {
	envBool, present := os.LookupEnv(string(env))

	b, e := strconv.ParseBool(envBool)
	if present && e != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Unable to convert environment variable '%s' to bool value was: '%s'", env, envBool)
		os.Exit(1)
	}

	if !present {
		return def
	} else {
		return bool(b)
	}
}

func (env Boolean) WithoutDefault() bool {
	envBool, present := os.LookupEnv(string(env))

	if !present {
		_, _ = fmt.Fprintf(os.Stderr, "Missing Boolean environment variable: %s", env)
		os.Exit(1)
	}

	b, e := strconv.ParseBool(envBool)
	if e != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Unable to convert environment variable '%s' to bool value was: '%s'", env, envBool)
		os.Exit(1)
	}

	return bool(b)
}
