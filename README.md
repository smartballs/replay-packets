# Replay Packets

## Setup development environment

* Docker _(v19.03.3 tested)_

If you want to compile the Go binary without Docker, you must donwload the library`libpcap-dev`

__On linux__

```$ sudo apt-get install -y libpcap-dev```

## Description

this tool allows you to replay network packets from files (.pcap)

by default it replays all files from a directory, but you can replay just one using the 'FILE_UNIQUE_PATH' environment variable

## Author

Guillaume Picard
guillaume.picard@ecole.ensicaen.fr

## Environment variables

| variable | description | default value |
| -------- | ----------- | ------------- |
| NETWORK_TYPE | udp or tcp | udp |
| ADDRESS_IP | IP address to send packets | 239.0.0.51 |
| ADDRESS_PORT | port address to send packets | 9000 |
| FILE_TYPE | type of files to replay | pcap |
| ASSETS_PATH | path to a directory containing files to replay | /usr/local/smartball/assets |
| FILE_DELAY | delay in seconds between each content file replayed | 1 |
| FILE_UNIQUE_PATH | path to a unique file to replay |  |

## Docker

### Build

```
docker build -t replay-packets .
```

### Run

```
docker run -it --name replay_packets replay-packets
```
