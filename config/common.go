package config

import (
	"gitlab.com/smartballs/replay-packets/pkg/cfg"
)

var Common = struct {
	NetworkType    string
	AddressIp      string
	AddressPort    int
	FileType       string
	FileDelay      int
	FileUniquePath string
	AssetsPath     string
}{
	NetworkType:    cfg.Str("NETWORK_TYPE").ByDefault("udp"),
	AddressIp:      cfg.Str("ADDRESS_IP").ByDefault("239.0.0.51"),
	AddressPort:    cfg.Num("ADDRESS_PORT").ByDefault(9000),
	FileType:       cfg.Str("FILE_TYPE").ByDefault("pcap"),
	FileDelay:      cfg.Num("FILE_DELAY").ByDefault(1),
	FileUniquePath: cfg.Str("FILE_UNIQUE_PATH").ByDefault(""), // If we set withoutDefault, program will crash
	AssetsPath:     cfg.Str("ASSETS_PATH").ByDefault("/usr/local/smartball/assets"),
}
